package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
)

type NFA struct {
	T []map[byte]int
	L []map[int]struct{}
	startingState int
	acceptingState int
}

func (ast *ParseTreeNode) buildNFA(alphabet Set) NFA {
	T := make([]map[byte]int, 0, 2)
	L := make([]map[int]struct{}, 0, 2)

	newState := func() int {
		T = append(T, make(map[byte]int))
		L = append(L, make(map[int]struct{}))
		return len(T)-1
	}

	startingState := newState()
	acceptingState := newState()

	var recur func(*ParseTreeNode, int, int)
	recur = func(node *ParseTreeNode, startState, endState int) {
		switch node.symbol {
		case "RE":
			recur(node.children[0], startState, endState)
		case "ALT":
			for _, child := range node.children {
				jump := newState()
				L[startState][jump] = struct{}{}
				recur(child, jump, endState)
			}
		case "SEQ":
			last := startState
			for _, child := range node.children[:len(node.children)-1] {
				next := newState()
				recur(child, last, next)
				last = next
			}
			recur(node.children[len(node.children)-1], last, endState)
		case "kleene":
			jump := newState()
			L[startState][jump] = struct{}{}
			recur(node.children[0], jump, jump)
			L[jump][endState] = struct{}{}
		case "plus":
			jump := newState()
			recur(node.children[0], startState, jump)
			recur(node.children[0], jump, jump)
			L[jump][endState] = struct{}{}
		case "dot":
			for c, _ := range alphabet {
				T[startState][c.(byte)] = endState
			}
		case "lambda":
			L[startState][endState] = struct{}{}
		case "char":
			T[startState][node.data] = endState
		default:
			panic(fmt.Errorf("unsupported AST symbol: %s", node.symbol))
		}
	}

	recur(ast, startingState, acceptingState)
	return NFA{T, L, startingState, acceptingState}
}

func (nfa NFA) write(fname string, lambda string, alphabet Set) {
	file, err := os.OpenFile(fname,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)
	writer.WriteString(fmt.Sprintf("%d %s", len(nfa.T), lambda))

	// write alphabet
	alphabetSlice := make([]byte, 0)
	for c, _ := range alphabet {
		alphabetSlice = append(alphabetSlice, c.(byte))
	}
	sort.SliceStable(alphabetSlice, func(i, j int) bool {
		return alphabetSlice[i] < alphabetSlice[j]
	})
	for _, c := range alphabetSlice {
		writer.WriteString(fmt.Sprintf(" %s", alphabetEncode(c)))
	}
	writer.WriteByte('\n')

	for fromState, m := range nfa.T {
		acceptingFlag := '-'
		if fromState == nfa.acceptingState {
			acceptingFlag = '+'
		}
		if len(m) == 0 {
			// ensure that states with no outgoing transitionns
			// (like the accepting state) have at least one line in
			// the .tt file
			writer.WriteString(fmt.Sprintf("%c %d %d\n",
				acceptingFlag, fromState, fromState))
		} else {
			for char, toState := range m {
				writer.WriteString(fmt.Sprintf("%c %d %d %s\n",
					acceptingFlag, fromState, toState,
					alphabetEncode(char)))
			}
		}
	}

	for fromState, m := range nfa.L {
		acceptingFlag := '-'
		if fromState == nfa.acceptingState {
			acceptingFlag = '+'
		}
		for toState, _ := range m {
			writer.WriteString(fmt.Sprintf("%c %d %d %s\n",
				acceptingFlag, fromState, toState, lambda))
		}
	}

	writer.Flush()
}

func (nfa NFA) draw(fname string) {
	file, err := os.OpenFile(fname,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	writer.WriteString("digraph{\n")

	for startState, m := range nfa.T {
		for char, endState := range m {
			var label string
			switch char {
			case '\n':
				label = "\\\\n"
			case ' ':
				label = "\\\\s"
			case '\\':
				label = "\\\\"
			default:
				label = string(char)
			}
			fmt.Fprintf(writer, "%d -> %d [label=\"%s\"]\n",
				startState, endState, label)
		}
	}

	for startState, m := range nfa.L {
		for endState, _ := range m {
			fmt.Fprintf(writer, "%d -> %d [label=\"λ\"]\n",
				startState, endState)
		}
	}

	writer.WriteString("}\n")
	writer.Flush()
}
