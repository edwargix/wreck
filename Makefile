GOSRC != find . -name '*.go'
GOSRC += go.mod
GO = go
RM ?= rm -f
GV_SOURCES = $(wildcard *.gv)
PNGS = $(GV_SOURCES:.gv=.png)

WRECK: $(GOSRC)
	$(GO) build -o $@

run: WRECK
	./WRECK scan.lut scan.u
	make pngs

grader: wreck-student.tar.bz2
	tar xvjf $<

%.png: %.gv
	dot -Tpng $< -o $@

pngs: $(PNGS)

clean:
	$(RM) -r WRECK *.gv *.png *.tt wreck/ scan.u

davidflorness.tar.gz: Makefile $(GOSRC) llre.cfg
	git archive -o $@ HEAD $^
