package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

func usage() {
	log.Fatalf("usage: %s lexing_config_file scanner_config_file",
		os.Args[0])
}

func main() {
	if len(os.Args) < 3 {
		usage()
	}

	lexingConfigFile, err := os.Open(os.Args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer lexingConfigFile.Close()
	alphabet, REs := readLexingConfFile(lexingConfigFile)

	scannerConfigFile, err := os.OpenFile(os.Args[2],
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer scannerConfigFile.Close()
	configWriter := bufio.NewWriter(scannerConfigFile)

	cfgFile, err := os.Open("llre.cfg")
	if err != nil {
		log.Fatal(err)
	}
	defer cfgFile.Close()

	grammar := readCFGFile(cfgFile)

	// write alphabet
	alphabetSlice := make([]byte, 0)
	for c, _ := range alphabet {
		alphabetSlice = append(alphabetSlice, c.(byte))
	}
	sort.SliceStable(alphabetSlice, func(i, j int) bool {
		return alphabetSlice[i] < alphabetSlice[j]
	})
	for _, c := range alphabetSlice {
		configWriter.WriteString(alphabetEncode(c))
	}
	configWriter.WriteByte('\n')

	// pick a lambda
	var lambda string
	for c := byte(1); c < 128; c++ {
		if !alphabet.Contains(c) {
			lambda = alphabetEncode(c)
			break
		}
	}
	if lambda == "" {
		panic(fmt.Errorf("could not find a lambda"))
	}

	for _, RE := range REs {
		if RE.optionalData != "" {
			configWriter.WriteString(fmt.Sprintf("%s.tt\t%s\t%s\n",
				RE.tokenID, RE.tokenID, RE.optionalData))
		} else {
			configWriter.WriteString(fmt.Sprintf("%s.tt\t%s\n",
				RE.tokenID, RE.tokenID))
		}

		tree := grammar.Parse([]byte(RE.str), alphabet)
		tree.draw(fmt.Sprintf("%s-concrete.gv", RE.tokenID))
		tree.concrete2ast()
		tree.draw(fmt.Sprintf("%s.gv", RE.tokenID))
		nfa := tree.buildNFA(alphabet)
		nfa.draw(fmt.Sprintf("%s-nfa.gv", RE.tokenID))
		nfa.write(fmt.Sprintf("%s.nfa", RE.tokenID), lambda, alphabet)
	}

	configWriter.Flush()
}
