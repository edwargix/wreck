package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type LLTable map[NonTerminal]map[Terminal]int

func (g Grammar) LLTable() LLTable {
	table := make(LLTable)
	for lhs, _ := range g.productions {
		table[lhs] = make(map[Terminal]int, g.terminals.Size())
		for t, _ := range g.terminals {
			table[lhs][t.(Terminal)] = -1
		}
	}

	for lhs, rhss := range g.productions {
		for i, _ := range rhss {
			for a, _ := range g.Predict(lhs, i) {
				if table[lhs][a.(Terminal)] != -1 {
					panic(fmt.Errorf("LL table conflict"))
				}
				table[lhs][a.(Terminal)] = i
			}
		}
	}

	return table
}

type ParseTreeNode struct {
	symbol string
	data byte
	parent *ParseTreeNode
	children []*ParseTreeNode
}

func (tree *ParseTreeNode) draw(fname string) {
	file, err := os.OpenFile(fname,
		os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0644)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	writer := bufio.NewWriter(file)

	var recur func(node *ParseTreeNode)
	recur = func(node *ParseTreeNode) {
		if node.data == 0 {
			fmt.Fprintf(writer, "\"%p\" [label=\"%s\"]\n", node, node.symbol)
		} else {
			fmt.Fprintf(writer, "\"%p\" [label=\"%s (%s)\"]\n", node,
				node.symbol,
				strings.ReplaceAll(string(node.data), "\\", "\\\\"))
		}
		for _, child := range node.children {
			fmt.Fprintf(writer, "\"%p\" -> \"%p\"\n", node, child)
			recur(child)
		}
	}

	writer.WriteString("digraph {\n")
	recur(tree)
	writer.WriteString("}\n")
	writer.Flush()
}

func (g Grammar) Parse(ts []byte, alphabet Set) *ParseTreeNode {
	lltable := g.LLTable()
	stack := make([]string, 0)
	ts = append(ts, '$')

	push := func(symbol string) {
		stack = append(stack, symbol)
	}

	pop := func() string {
		s := stack[len(stack)-1]
		stack = stack[0:len(stack)-1]
		return s
	}

	// returns the type of terminal (token type), the data associated with
	// it (0 if none), and the number of chars eaten from the top of the
	// tokens stream
	char2terminal := func() (Terminal, byte, int) {
		switch {
		case ts[0] == '|':
			return "pipe", 0, 1
		case ts[0] == '(':
			return "open", 0, 1
		case ts[0] == ')':
			return "close", 0, 1
		case ts[0] == '*':
			return "kleene", 0, 1
		case ts[0] == '+':
			return "plus", 0, 1
		case ts[0] == '.':
			return "dot", 0, 1
		case ts[0] == '-':
			return "dash", 0, 1
		case ts[0] == '$':
			return "$", 0, 1
		case ts[0] == '\\':
			var data byte
			switch ts[1] {
			case 'n':
				data = '\n'
			case 's':
				data = ' '
			default:
				data = ts[1]
			}
			if !alphabet.Contains(data) {
				panic(fmt.Errorf("char not in alphabet: %c", data))
			}
			return "char", data, 2
		case alphabet.Contains(ts[0]):
			return "char", ts[0], 1
		default:
			panic(fmt.Errorf("no terminal for char: %c", ts[0]))
		}
	}

	root := &ParseTreeNode{
		symbol: "",
		parent: nil,
		children: make([]*ParseTreeNode, 0, 1),
	}
	cur := root
	push(string(g.start))
	for len(stack) > 0 {
		x := pop()
		if g.nonTerminals.Contains(NonTerminal(x)) {
			NT := NonTerminal(x)
			T, _, _ := char2terminal()
			p := lltable[NT][T]
			if p == -1 {
				panic(fmt.Errorf("syntax error"))
			}
			push("")
			rhs := g.productions[NT][p]
			for i := len(rhs)-1; i >= 0; i-- {
				push(rhs[i])
			}
			cur.children = append(cur.children, &ParseTreeNode{
				symbol: x,
				parent: cur,
				children: make([]*ParseTreeNode, 0, 1),
			})
			cur = cur.children[len(cur.children)-1]
		} else if g.terminals.Contains(Terminal(x)) {
			T, data, n := char2terminal()
			if Terminal(x) != T {
				panic(fmt.Errorf("unexpected character: %c", ts[0:n]))
			}
			cur.children = append(cur.children, &ParseTreeNode{
				symbol: x,
				data: data,
				parent: cur,
				children: nil,  // terminal should never have children
			})
			ts = ts[n:]
		} else if x == "" { // marker
			cur = cur.parent
		}
	}
	if len(root.children) != 1 {
		panic(fmt.Errorf("dummy root node has %d children instead of 1", len(root.children)))
	}
	root.children[0].parent = nil
	return root.children[0]
}

func (g Grammar) Predict(lhs NonTerminal, index int) Set {
	ans := g.First(g.productions[lhs][index])
	if g.ruleDerivesEmpty[lhs][index] {
		for f, _ := range g.Follow(lhs) {
			ans.Add(f.(Terminal))
		}
	}
	return ans
}
