package main

import (
	"fmt"
)

func alphabetEncode(b byte) string {
	switch {
	case (b >= 'a' && b <= 'z' && b != 'x') || (b >= 'A' && b <= 'Z'):
		return string(b)
	default:
		return fmt.Sprintf("x%02X", b)
	}
}
