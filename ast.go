package main

import (
	"fmt"
)

func (node *ParseTreeNode) indexInParentChildren() int {
	for i, child := range node.parent.children {
		if child == node {
			return i
		}
	}
	panic(fmt.Errorf("node not in parent's children"))
}

func (node *ParseTreeNode) replaceWith(other *ParseTreeNode) {
	other.parent = node.parent
	node.parent.children[node.indexInParentChildren()] = other
}

func (node *ParseTreeNode) delete() {
	i := node.indexInParentChildren()
	node.parent.children = append(node.parent.children[:i],
		node.parent.children[i+1:]...)
}

func (node *ParseTreeNode) concrete2ast() {
	switch node.symbol {
	case "RE":
		node.children[0].concrete2ast()
		node.children[1].delete()
	case "ALT", "ALTLIST", "SEQ", "SEQLIST":
		if len(node.children) == 0 {
			node.delete()
		} else {
			// SEQ/SEQLIST: ATOM SEQLIST
			// ALT: SEQ ALTLIST
			// ALTLIST: pipe SEQ ALTLIST
			if node.children[0].symbol == "pipe" {
				node.children = node.children[1:]
			}
			node.children[0].concrete2ast()
			// child 0 may get deleted
			node.children[len(node.children)-1].concrete2ast()
			if len(node.children) == 0 {
				if node.symbol == "ALTLIST" {
					node.replaceWith(&ParseTreeNode{
						symbol: "lambda",
					})
				} else {
					node.delete()
				}
			} else if len(node.children) == 1 {
				node.replaceWith(node.children[0])
			} else {
				rightChild := node.children[1]
				if rightChild.symbol == node.symbol[0:3] {
					// rm right child
					node.children = node.children[0:1]
					// transfer right child's children to
					// this node's
					for _, child := range rightChild.children {
						child.parent = node
						node.children = append(node.children, child)
					}
				}
				// SEQLIST -> SEQ
				// ALTLIST -> ALT
				node.symbol = node.symbol[0:3]
			}
		}
	case "ATOM":
		node.children[0].concrete2ast()
		node.children[1].concrete2ast()
	case "ATOMMOD":
		if len(node.children) == 0 {  // lambda
			node.delete()
			node.parent.replaceWith(node.parent.children[0])
		} else {
			// kleen or plus
			child := node.children[0]
			if child.children != nil {
				panic(fmt.Errorf("terminal children should be nil"))
			}
			child.children = node.parent.children[0:1]
			node.parent.replaceWith(child)
		}
	case "NUCLEUS":
		switch node.children[0].symbol {
		case "open":
			// remove parentheses
			node.children = node.children[1:2]
			node.children[0].concrete2ast()
			node.replaceWith(node.children[0])
		case "char":
			// check if CHARRNG is not lambda
			charrng := node.children[1]
			if len(charrng.children) > 0 {
				// do char range
				alt := &ParseTreeNode{
					symbol: "ALT",
					children: make([]*ParseTreeNode, 0, 2),
				}
				for char := node.children[0].data;
					char <= charrng.children[1].data;
					char++ {
					alt.children = append(alt.children, &ParseTreeNode{
						symbol: "char",
						data: char,
						parent: alt,
					})
				}
				node.replaceWith(alt)
			} else {
				node.replaceWith(node.children[0])
			}
		case "dot":
			node.replaceWith(node.children[0])
		}
	case "CHARRNG":
		panic(fmt.Errorf("shouldn't ever reach CHARRNG in tree optimizer"))
	}
}
