package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
)

type RawRE struct {
	str string
	tokenID string
	optionalData string
}

// returns alphabet and REs
func readLexingConfFile(f *os.File) (Set, []RawRE) {
	alphabet := make(Set)
	lineScanner := bufio.NewScanner(f)
	REs := make([]RawRE, 0)

	lineScanner.Scan()

	// read first line
	parts := strings.FieldsFunc(lineScanner.Text(), unicode.IsSpace)
	if len(parts) == 0 {
		panic(fmt.Errorf(
			"the first line of the lexing config file cannot be empty"))
	}
	for _, p := range parts {
		for i := 0; i < len(p); i++ {
			if p[i] == 'x' {
				// alphabet encoding
				c, err := strconv.ParseInt(p[i+1:i+3], 16, 8)
				if err != nil {
					panic(err)
				}
				alphabet.Add(byte(c))
				i += 2
			} else {
				alphabet.Add(p[i])
			}
		}
	}

	for lineScanner.Scan() {
		// split line by whitespace
		parts = strings.FieldsFunc(lineScanner.Text(), unicode.IsSpace)
		l := len(parts)
		if l == 0 {
			continue
		}
		if l < 2 || l > 3 {
			panic(fmt.Errorf(
				"there must be 2 or 3 fields per line in scan definition file; one has %d",
				l))
		}
		if (l == 2) {
			REs = append(REs, RawRE {
				str: parts[0],
				tokenID: parts[1],
				optionalData: "",
			})
		} else { // 3
			REs = append(REs, RawRE {
				str: parts[0],
				tokenID: parts[1],
				optionalData: parts[2],
			})
		}
	}
	return alphabet, REs
}
