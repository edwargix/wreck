package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
	"unicode"
)

type Grammar struct {
	start              NonTerminal
	productions        map[NonTerminal][]RHS
	nonTerminals       Set
	terminals          Set
	symbolDerivesEmpty map[NonTerminal]bool
	// maps production to bool indicating whether production derives to
	// lambda. The array needs the index of the production in the value of
	// `productions'
	ruleDerivesEmpty   map[NonTerminal][]bool
}
type NonTerminal string			// nonterminal
type Terminal string
type RHS []string

func nonTerminals(productions map[NonTerminal][]RHS) Set {
	NTs := make(Set)
	for lhs, _ := range productions {
		NTs.Add(lhs)
	}
	return NTs
}

func terminals(productions map[NonTerminal][]RHS) Set {
	Ts := make(Set)
	for _, rhss := range productions {
		for _, rhs := range rhss {
			for _, sym := range rhs {
				if len(sym) == 0 {
					panic(fmt.Errorf("empty smybol in grammar"))
				}
				// allow lowercase and $
				if (sym[0] >= 97 && sym[0] <= 122) || sym[0] == '$' {
					Ts.Add(Terminal(sym))
				}
			}
		}
	}
	return Ts
}

func (g Grammar) productionLocationOf(lhs NonTerminal, index int, X string) int {
	for i, sym := range g.productions[lhs][index] {
		if sym == X {
			return i
		}
	}
	return -1
}

func (g *Grammar) derivesEmptyString() {
	g.symbolDerivesEmpty = make(map[NonTerminal]bool)
	// maps production to bool/int respectively
	// int is index of production
	g.ruleDerivesEmpty = make(map[NonTerminal][]bool)

	count := make(map[NonTerminal][]int)
	workList := make(Set)

	checkForEmpty := func (lhs NonTerminal, i int) {
		if count[lhs][i] == 0 {
			g.ruleDerivesEmpty[lhs][i] = true
			if !g.symbolDerivesEmpty[lhs] {
				g.symbolDerivesEmpty[lhs] = true
				workList.Add(lhs)
			}
		}
	}

	for A, _ := range g.nonTerminals {
		g.symbolDerivesEmpty[A.(NonTerminal)] = false
	}
	for lhs, rhss := range g.productions {
		g.ruleDerivesEmpty[lhs] = make([]bool, len(g.productions[lhs]))
		count[lhs] = make([]int, len(g.productions[lhs]))
		for i, rhs := range rhss {
			// g.ruleDerivesEmpty[lhs][i] = false
			count[lhs][i] = len(rhs)
			checkForEmpty(lhs, i)
		}
	}
	for workList.Size() > 0 {
		X := workList.Pop().(NonTerminal)
		for lhs, rhss := range g.productions {
			for i, _ := range rhss {
				if g.productionLocationOf(lhs, i, string(X)) != -1 {
					count[lhs][i]--
					checkForEmpty(lhs, i)
				}
			}
		}
	}
}

func (g Grammar) First(a []string) Set {
	firstSet := make(Set)
	if len(a) == 0 {
		return firstSet
	}
	if g.terminals.Contains(Terminal(a[0])) {
		firstSet.Add(Terminal(a[0]))
		return firstSet
	}

	// a[0] must be a NonTerminal

	visitedFirst := make(map[NonTerminal]bool)
	for A, _ := range g.nonTerminals {
		visitedFirst[A.(NonTerminal)] = false
	}

	var internalFirst func([]string)
	internalFirst = func(symbols []string) {
		if len(symbols) == 0 {
			return
		}
		X := symbols[0]
		if g.terminals.Contains(Terminal(X)) {
			firstSet.Add(Terminal(X))
			return
		}
		// X must be a NonTerminal
		NT := NonTerminal(X)
		if !visitedFirst[NT] {
			visitedFirst[NT] = true
			for _, rhs := range g.productions[NT] {
				internalFirst(rhs)
			}
		}
		if g.symbolDerivesEmpty[NT] {
			internalFirst(symbols[1:])
		}
	}

	internalFirst(a)
	return firstSet
}

func (g Grammar) AllDeriveEmpty(y []string) bool {
	for _, X := range y {
		if g.terminals.Contains(X) || !g.symbolDerivesEmpty[NonTerminal(X)] {
			return false
		}
	}
	return true
}

func (g Grammar) Follow(A NonTerminal) Set {
	if len(A) == 0 {
		panic(fmt.Errorf("empty string passed to Follow"))
	}
	followSet := make(Set)

	visitedFollow := make(map[NonTerminal]bool)
	for A, _ := range g.nonTerminals {
		visitedFollow[A.(NonTerminal)] = false
	}

	var internalFollow func(NonTerminal)
	internalFollow = func(A NonTerminal) {
		if !visitedFollow[A] {
			visitedFollow[A] = true
			for lhs, rhss := range g.productions {
				for i, rhs := range rhss {
					l := g.productionLocationOf(lhs, i, string(A))
					if l != -1 {
						for f, _ := range g.First(rhs[l+1:]) {
							followSet.Add(f.(Terminal))
						}
						if g.AllDeriveEmpty(rhs[l+1:]) {
							internalFollow(lhs)
						}
					}
				}
			}
		}
	}

	internalFollow(A)
	return followSet
}

func (g *Grammar) removeLambdasFromProductions() {
	for lhs, rhss := range g.productions {
		for i, rhs := range rhss {
			if g.productionLocationOf(lhs, i, "lambda") != -1 {
				if len(rhs) > 1 {
					panic(fmt.Errorf(
						"production with >1 symbols contains lambda"))
				}
				g.productions[lhs][i] = make([]string, 0)
			}
		}
	}
}

func readCFGFile(f *os.File) Grammar {
	// maps symbol (lhs) to slice of slices of strings (rhs)
	productions := make(map[NonTerminal][]RHS)
	lineScanner := bufio.NewScanner(f)
	var firstSymbol NonTerminal

	var lhs string
	for lineScanner.Scan() {
		// split line by whitespace
		parts := strings.FieldsFunc(lineScanner.Text(), unicode.IsSpace)
		if parts[0] == "|" {
			if lhs == "" {
				panic(fmt.Errorf(
					"came across alternation with no "+
					"preceding LHS symbol in CFG file",
				))
			}
			// productions[lhs] must be an allocated slice since
			// this is an alternation
			if parts[1] == "|" {
				panic(fmt.Errorf(
					"cannot begin RHS of production with |"))
			}
			var i, j int
			j = 1
			for i = 2; i < len(parts); i++ {
				if parts[i] == "|" {
					productions[NonTerminal(lhs)] = append(
						productions[NonTerminal(lhs)], parts[j:i])
					j = i+1
				}
			}
			productions[NonTerminal(lhs)] = append(productions[NonTerminal(lhs)], parts[j:i])
		} else {
			lhs = parts[0]
			if firstSymbol == "" {
				firstSymbol = NonTerminal(lhs)
			}
			// start at 2 to skip "->"
			if parts[2] == "|" {
				panic(fmt.Errorf(
					"cannot begin RHS of production with |"))
			}
			var i, j int
			j = 2
			for i = 3; i < len(parts); i++ {
				if parts[i] == "|" {
					productions[NonTerminal(lhs)] = append(
						productions[NonTerminal(lhs)], parts[j:i])
					j = i+1
				}
			}
			productions[NonTerminal(lhs)] = append(productions[NonTerminal(lhs)], parts[j:i])
		}
	}

	g := Grammar {
		start: firstSymbol,
		productions: productions,
		nonTerminals: nonTerminals(productions),
		terminals: terminals(productions),
	}
	g.removeLambdasFromProductions()
	g.derivesEmptyString()

	return g
}
